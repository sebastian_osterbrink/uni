package de.upb.ml1;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;


public class QuadraticRegressor {
	
	private RealVector wVector ; 
	private RealMatrix wMatrix; 
	
	private List<RealVector> trainingData ; 
	
	private int dim ; 
	private double lambda = 1; 
	
	public QuadraticRegressor(int n) { 
		dim = n; 
		wVector = new ArrayRealVector(n+1);
		wMatrix = new Array2DRowRealMatrix(n, n); 
		
		trainingData = new ArrayList<RealVector>(); 
		
	}

	public void addTrainingExample(RealVector x, double y) {
		trainingData.add(x.append(y)); 		
	}

	public void train() {
		
	}
	
	public void exportToFile(File f) throws IOException{ 
		PrintWriter writer = new PrintWriter(f, "UTF-8");
		CSVPrinter out = new CSVPrinter(writer, CSVFormat.DEFAULT); 
		
		for( int k = 0 ; k <= 78 ; k++ )  { 
			writer.print("w_"+k+",");
		}
		writer.println();

		System.out.println(trainingData.size());
		
		for (RealVector x : trainingData){ 
			RealVector exportVector = x ; 
			for(int i = 0; i< dim ; i++ ) { 
				for(int j = i ; j< dim; j++ ) {  
					exportVector = exportVector.append(x.getEntry(i) * x.getEntry(j)); 
				}
			}
			for( int k = 0 ; k < exportVector.getDimension() ; k++ )  { 
				writer.print(exportVector.getEntry(k)+",");
			}
			writer.println();
//			out.printRecord(exportVector);
		}
		writer.close();
//		out.close();
		
		
	}
	
 	public double getEIn() { 
		double error = 0; 
		for (RealVector x : trainingData){ 
			error += Math.pow(estimate(x) - x.getEntry(x.getDimension()),2); 
		}
		return error / trainingData.size(); 
	}
	
	public double getEAug(){ 
		double e = 0; 
		for (int i = 0; i < dim; i++) {
			e += Math.pow(wVector.getEntry(i),2); 
		}
		for (int i = 0; i < dim; i++) { 
			for (int j = i; j < dim; j++) {
				e += Math.pow(wMatrix.getEntry(i,j),2); 
			}
		}
		return lambda * e + getEIn();
	}
	
	public double estimate(RealVector x ) {
		double fx = wVector.getEntry(0); 
		for (int i = 1; i < wVector.getDimension(); i++) {
			fx += wVector.dotProduct(x); 
		}
		for (int i = 0; i < wMatrix.getRowDimension(); i++) {
			for (int j = i; j < wMatrix.getColumnDimension(); j++) {
				fx += wMatrix.getEntry(i, j) * x.getEntry(i) * x.getEntry(j); 
			}			
		}
		return fx; 
	}

}
