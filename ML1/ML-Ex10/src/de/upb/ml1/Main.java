package de.upb.ml1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

public class Main {

	public static final int DIMENSION = 11; 
	
	public static void main(String[] args) throws IOException {
		
		Reader in = new FileReader("data10.csv");
		CSVFormat hwFormat = CSVFormat.newFormat(' '); 
		hwFormat = hwFormat.withIgnoreSurroundingSpaces(true); 
		hwFormat = hwFormat.withIgnoreEmptyLines(); 
		Iterable<CSVRecord> records = CSVFormat.DEFAULT.withHeader().withIgnoreSurroundingSpaces().parse(in);

		
		QuadraticRegressor learner = new QuadraticRegressor(DIMENSION); 

		int count = 0 ; 
		
		for (CSVRecord record : records) {
			double[] values = new double[DIMENSION]; 
			for (int i = 0; i < values.length; i++) {
				values[i]  = Double.parseDouble(record.get(i)); 
			}
			double  y = Double.parseDouble(record.get(record.size()-1)); 
			RealVector x = new ArrayRealVector(values); 

			learner.addTrainingExample( x, y);
//			System.out.println(record);
//			System.out.println(x + " ; " + y );
			count++; 
		}
		System.out.println(count);
		
		learner.exportToFile(new File("out.csv"));
		
		System.out.println(learner);
	}

}
