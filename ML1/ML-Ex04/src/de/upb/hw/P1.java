package de.upb.hw;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class P1 {
	
	
	private static Map<String, Coin> coins;
	private static Coin coinA ;
	private static Coin coinB ; 
	private static Coin coinC ; 



	public static void main(String args[]) {
		
		coinA =  new Coin("A", 0.25); 
		coinB = new Coin("B", 0.5); 
		coinC = new Coin("C", 0.75); 

		Purse purse = new Purse(); 
		purse.addCoins(coinA, 20); 
		purse.addCoins(coinB, 30);
		purse.addCoins(coinC, 50); 
		
		int runs =10 ; 

//		for (int j = 0; j <= runs; j++) {
//			System.out.println(runs+": " + j+  " " + coinC.getProb(j, runs) );			
//		}

		Double[][] eOut = new Double[runs][runs]; 
		for (int a = 0; a <= eOut.length; a++) {
			for (int b = a  ; b < eOut.length; b++) {
				Double val = 0.0;
				for (int x = 0; x <= a; x++) {
					val +=  0* coinA.getProb(x,runs) + 2 * coinB.getProb(x, runs) + 1*  coinC.getProb(x,runs); 
				}
				for (int x = a+1; x <= b; x++) {
					val +=  4* coinA.getProb(x,runs) + 0 * coinB.getProb(x, runs) + 2 * coinC.getProb(x,runs); 
				}
				for (int x = b+1; x <= 10; x++) {
					val +=  6* coinA.getProb(x,runs) + 3 * coinB.getProb(x, runs) + 0* coinC.getProb(x,runs); 					
				}
				eOut[a][b]= val; 
			}
		}
		
		int ma = 0; 
		int mb = 1;
		double  min = Integer.MAX_VALUE; 
		for (int a = 0; a <= runs; a++) {
			for (int b = a + 1 ; b < runs; b++) {
				Double val = eOut[a][b]; 
				if ( val < min ) { 
					ma = a ; 
					mb = b ; 
					min = val; 
				}
			}
		}
		System.out.println(ma + " "+mb +" "+ min );
		exportEOut(new File("eout.tex"), eOut);
 
	}	
	
	
	private static void exportEOut(File file,  Double[][] eOut) { 
		PrintWriter writer;
		try {
			writer = new PrintWriter(file, "UTF-8");
			for (int b = 0 ; b < eOut.length; b++) {
				writer.print( " & " + b  );
			}			
			writer.println(" \\\\ ");
			for (int a = 0; a < eOut.length; a++) {
				writer.print(  a  );
				for (int b = 0 ; b < eOut.length; b++) {
					double val = eOut[a][b] != null ? Math.round(100 * eOut[a][b]): 0; 
					writer.print( " & " + (val/ 100)  );
				}
				writer.println(" \\\\ ");
			}
			writer.close(); 
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}	
	}

	
}
