package de.upb.hw;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;

public class Purse  {
	

	private Map<Coin, Integer> coins;
	private Random rand; 
	
	public Purse() { 
		coins = new HashMap<Coin,Integer>(); 
		rand = new Random(); 
	}

	public void addCoins(String name, double prob, int number){ 
		coins.put(new Coin(name, prob),number); 
	}
	
	public void addCoins(Coin coin, int i) {
		coins.put(coin, i); 
	}
	
	public void addCoins(Map<Coin, Integer> coins) {
		this.coins.putAll(coins); 
	}
	
	public Coin drawRandomCoin(){ 
		int size = 0; 
		for (Coin c: coins.keySet()) { 
			size +=  coins.get(c); 
		}
		int item = rand.nextInt(size); 
		int i =0 ; 
		for (Coin c: coins.keySet()) { 
			if (i <= item && item <= i + coins.get(c) )
				return c; 
			i += coins.get(c); 
		}
		return null; 
	}


	
}
