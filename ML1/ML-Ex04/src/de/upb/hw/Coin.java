package de.upb.hw;

public class Coin {
	
	private String className; 
	private double prob; 
	
	
	public Coin(String className, double prob) { 
		this.className = className; 
		this.prob = prob; 
	}
	
	/** 
	 * Toss the coin 
	 * @return true if it lands heads-up 
	 */
	public boolean toss(){ 
		return Math.random() < prob; 
	}
	
	/** 
	 * compare the className attribute
	 * @param s
	 * @return
	 */
	public boolean isOfClass(String s) { 
		return className.equals(s); 
	}
	
	public double getProb(){ 
		return prob; 
	}
	
	private long calcBinom(int n, int k ) { 
		long result = 1;  
		int m = n-k > k ? k : n-k ; 
		for (int i = 1; i <= m; i++) {
			result *= n  ;
			result /= i; 
			n--; 
		}		
		return result; 
	}
	
	public double getProb(int k,  int n ){
//		System.out.print(calcBinom(n,k)+" " );
		double result = Math.pow(prob , k) * Math.pow(1-prob , n-k) * calcBinom(n, k); 		
		return result; 
	}
}
