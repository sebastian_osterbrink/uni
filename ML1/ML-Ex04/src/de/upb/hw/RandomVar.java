package de.upb.hw;

import java.util.HashMap;
import java.util.Map;

public class RandomVar {

	private Double mu; 
	
	public RandomVar(int i) { 
		double factor = i;  
		mu = 0.25  + factor / 200; 
	}
	
	public boolean eval(double x) { 
		return x < mu ; 
	}
	
	public double getMu(){ 
		return mu; 
	}
	
	public Map<Double,Integer> getRealisations(int n) { 
		Map<Double, Integer> result = new HashMap<Double, Integer>(); 
		for (int i = 0; i < n; i++) {
			double x = Math.random(); 
			result.put(x, eval(x) ? 1 : 0); 
		}
		return result; 				
	}

	public String toString() { 
		return ""+ mu;  
	}
}
