package de.upb.hw;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;

public class P2 {

	static RandomVar[] randomVars; 
	static double[] v; 
	static int hNum = 100 ; 
	static int N = 100; 
	
	
	public static void main(String args[]) { 
		randomVars = new RandomVar[hNum];
		for (int i = 0; i < hNum; i++) {
			RandomVar xi = new RandomVar(i); 
			randomVars[i] = (xi); 
//			System.out.println(xi);
		}

		v = new double[hNum]; 
		for (int i = 0; i < v.length; i++) {
			v[i] = 0; 
			Map<Double, Integer> sample = randomVars[i].getRealisations(N); 
			for(Double x : sample.keySet()){ 
				v[i] +=  sample.get(x); 
			}
			v[i] = v[i]/N; 
		}
	
		PrintWriter writer;
		try {
			writer = new PrintWriter("2a.csv", "UTF-8");
			writer.println("i x y"); 
			for (int i = 0; i < v.length; i++) {
				writer.println(i + " " +  randomVars[i] + " " + v[i] );
			}
			writer.close();
		} catch (Exception e) { 
		}
		
		System.out.println("v_" + findMinV()+ " is minimal ");
	}


	private static Integer findMinV() {
		List<Integer> minPos = new ArrayList<Integer>(); 
		double min = Double.POSITIVE_INFINITY; 
		for (int i = 0; i < v.length; i++) {
			if (min > v[i] ) { 
				min = v[i]; 
				minPos.clear(); 
				minPos.add(new Integer(i)); 
			}
			if (min == v[i]){ 
				minPos.add(new Integer(i)); 				
			}
		}
		
		return minPos.get(0);
	}
	
}
