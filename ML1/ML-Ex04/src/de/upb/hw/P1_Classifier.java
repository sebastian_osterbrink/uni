package de.upb.hw;

public class P1_Classifier {
	
	private int a; 
	private int b; 
	
	public P1_Classifier(int a, int b) { 
		this.a = a;
		this.b  =  b; 
	}
	
	public String classify(int p) { 
		if (p <= a) { 
			return "A"; 
		} else if (p<= b ) { 
			return "B";
		} else { 
			return "C"; 
		}
	}
	
}
