package de.upb.ml1;

import java.io.File;

import org.apache.commons.math3.distribution.NormalDistribution;

public class Main {

	public static void main(String[] args) {
		p1(); 
	}

	
	private static void p1() {
		
		P1Function p1Functions = new P1Function() ; 
		NormalDistribution epsilon = new NormalDistribution(0 , 0.5); 
		
		double globalB = 0; 
		double[] globalCoeff = { 0,0,0,0,0 } ; 
		
		int runs = 100 ; 
		for (int j = 0; j < runs; j++) {
			p1Functions.resetTrainingData(); 
			
			int N = 10; 
			for ( int i = 0 ; i < N ; i++ ) { 
				double x = Math.random() * 10 ; 
				double y = x + epsilon.sample(); 
				
				p1Functions.addTrainingData(x, y);
			}
			
			double b  = p1Functions.g1Training(); 
			double[] coeff= p1Functions.g2Training(); 
			
			globalB += b ; 
			for (int i = 0; i < coeff.length; i++) {
				globalCoeff[i] += coeff[i]; 
			}
		}
		
		System.out.println(globalB / runs );
		System.out.print(0);
		for (int i = 0; i < globalCoeff.length; i++) {
			System.out.print( "+ x^"+ i + " * " + globalCoeff[i] / runs); 
		}

		

		
//		g1.writeDataToFile(new File("1a.csv")); 
//		g1.writeG2ToFile(new File("g2.csv"));
//		g1.writePredictionsToFile(new File("1aPred.csv"));

//		g1.writeDataToFile(new File("E:\\uni\\ML1\\hw7\\1a.csv")); 
//		g1.writeG2ToFile(new File("E:\\uni\\ML1\\hw7\\g2.tex")); 
//		g1.writePredictionsToFile(new File("E:\\uni\\ML1\\hw7\\1aPred.csv"));

	}

}
