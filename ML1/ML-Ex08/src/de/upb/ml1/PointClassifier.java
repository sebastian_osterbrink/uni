package de.upb.ml1;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PointClassifier {
		
	
	private static double epsilon = 0.1 / 100 ; 
//	private static double delta = 0.1 ; 
	
	
	public static void main(String[] args ) throws FileNotFoundException, UnsupportedEncodingException{ 
		
	
		Function staticClassifier =  new ClassifierP2() {			
			private int t =0 ; 
			private int f = 0;
			
			@Override
			public void train(Point p, Boolean b) {
				if (trueEvaluation(p)) { 
					t++; 
				} else { 
					f++;
				}
			}
			@Override
			public Boolean evalPoint(Point p) {
				return t > f;
			}
		};
		
		
		
		
	}
			
//			for(Integer trainingSize  = 0 ; trainingSize < 100; trainingSize++){  				
//
//				// Generate a training data set of increasing size 
//				List<Point> trainingData = makePoints(steps );
//				for(Point p : trainingData) { 
//					f.train(p, f.trueEvaluation(p));
//				}
//				
//				int errors = 0;
//				int testSize =10000; 
//				List<Point> outData = makePoints(testSize); 
//				for(Point p : outData) { 
//					if(f.evalPoint(p) != f.trueEvaluation(p)){ 
//						errors++; 
//					}
//				}	
//				
//				double eOut =  (double) errors / ( testSize) ; 
////				System.out.println("run "+ r + " training size " + trainingSize + " errors: " + errors);
//				
//				// is the number of errors bigger than epsilon 
//				if ( eOut > epsilon ) { 
////					System.out.print(r+" "+trainingSize + "" );
////					System.out.println(" "+trainingSizeReached.containsKey(trainingSize)) ; 
//					Integer reachedNumber = 
//							trainingSizeReached.containsKey(trainingSize) ? 
//									trainingSizeReached.get(trainingSize) : 0 ; 					
//					trainingSizeReached.put(trainingSize, reachedNumber+1); 					
//				}
//				
//				// Save the number of errors 
//				eOut += results.containsKey(trainingSize) ? results.get(trainingSize) : 0 ; 
//				results.put(trainingSize, eOut); 
//			}
//		}
//
//		PrintWriter writer = new PrintWriter(new File("2a.csv"), "UTF-8");		
//		writer.println("n eout perc");
//		int t = 1; 
//		while ( results.containsKey(t)){ 
//			double result = round( (100.0 ) * results.get(t) /N  ) ; 
//			int passed = trainingSizeReached.containsKey(t) ? trainingSizeReached.get(t) : 0  ; 
//			String output = t*steps +" " + result +" "+ passed; 
////			System.out.println(output);
//			writer.println(output);
//			t++; 
//		}
//		writer.close(); 
//		
//		
		

//	}


	public static List<Point> makePoints(int n){ 
		List<Point> data = new ArrayList<Point>();
		for (int i = 0; i < n; i++) {
			Point p = new Point(); 
			data.add(p); 			
		}		
		return data ; 
	}
	

	public static double round(double d) { 
		return roundTo(d, 3); 
	}
	
	public static double roundTo(double d, int n) { 
		double result = d ; 
		long factor = Math.round(Math.pow(10, n)) ; 
		result = result * factor; 
		result = Math.round(result) ;
		result = result / factor; 
		return result ; 
	}
	
	
	
	
	
	
}
