package de.upb.ml1;

public class Point {

	
// Static 
	
 public static double maxX = 2; 
 public static double maxY = 2; 
 
 public static Point origin = new Point(-1,-1); 

 public static void setMax(double mx, double my) { 
	 maxX = mx ; 
	 maxY = my; 
 }
 
 public static void setOrigin(Point p){ 
	 origin = p ; 
 }
 

 
 // Object 
 
 public double x, y ; 
 
 public Point() { 
  this.x = Point.origin.x + Math.random() * maxX; 
  this.y = Point.origin.y + Math.random() * maxY; 
 }
 
 public Point(double x, double y) { 
	  this.x = x ; 
	  this.y = y; 
 }

 public double calculateDistance(Point b) { 
  return Math.sqrt((x - b.x) *(x - b.x)  + (y - b.y)*(y - b.y) ); 
 }

 public String toString(){ 
  return " "+ x + " , " + y+ "  ";   
 }
	

}
