package de.upb.ml1;


public abstract class Function<R> {

	public abstract R evalPoint(Point p);

	public abstract R trueEvaluation(Point p);

	public abstract void  train(Point p,R b); 
	

}
