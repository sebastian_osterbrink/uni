package de.upb.ml1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.math3.fitting.PolynomialCurveFitter;
import org.apache.commons.math3.fitting.WeightedObservedPoints;

public class P1Function {
	
	Map<Double, Double> trainingData = new HashMap<Double,Double>(); 
	Double g1b; 
	double[] coeff; 
	
	public double trueEval(double d) { 
		return d; 
	}
	
	public void addTrainingData(Double point, Double value) { 
		trainingData.put(point, value); 
	}
	
	public void resetTrainingData(){ 
		trainingData.clear(); 
	}
	
	public double[] g2Training() { 
		
        final WeightedObservedPoints obs = new WeightedObservedPoints();
		for (double x :trainingData.keySet()){
            obs.add(x, trainingData.get(x));
        }
        final PolynomialCurveFitter fitter = PolynomialCurveFitter.create(4);
        coeff = fitter.fit(obs.toList()); 
               
        return coeff; 
	}
	
	public double g1Training(){ 
		g1b = 0.0; 
		for(double xi : trainingData.keySet() ) { 
			g1b += trainingData.get(xi) ; 
		}
		g1b = g1b / trainingData.size(); 
		return g1b; 
	}
	
	public double g1Prediction(double x) { 
		return g1b ; 
	}
	
	public double g2Prediction(double x) { 
		double result = 0 ; 
		for(int i = 0 ; i < coeff.length ; i ++ ) { 
			result += Math.pow(x, i) * coeff[i]; 
		}
		return result ; 
	}
	
	
//		double ssxy = 0;  
//		double n = trainingData.size() ; 
//		double ssxx = 0  ; 
//		
//		for(double xi : trainingData.keySet() ) { 
//			ssxy += n * xi * trainingData.get(xi) ; 
//			for(double yi : trainingData.keySet() ) { 
//				ssxy -= xi * trainingData.get(yi);
//			}
//		}
//
//		double h = 0 ; 
//		for(double xi : trainingData.keySet()){ 
//			ssxx += n * xi * xi ; 
//			h += xi ; 
//		}
//		ssxx -= (h * h); 
//		
//		g1b = ssxy / ssxx; 
	
	public String toString() { 
		String result = ""; 
		for(double xi : trainingData.keySet() ) { 
			result = result + " (" + xi + " " + trainingData.get(xi)+ ") " ; 
		}
		return result + g1b ; 
	}

	public void writePredictionsToFile(File file) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(file, "UTF-8");
			writer.println("x f g1 g2 g1error g2error");
			for(double i = 1 ; i < 1000 ; i ++ ) { 
				double x  = i /100 ;
				writer.println(x + " " + x + " " + g1Prediction(x) + " " + g2Prediction(x) + " " 
							+ Math.abs(x - g1Prediction(x)) + " " 
							+ Math.abs(x - g2Prediction(x))); 
			}
			writer.close(); 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}		
	}

	public void writeG2ToFile(File file) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(file, "UTF-8");
			writer.print("0");
			for(int i = 0 ; i < coeff.length ; i ++ ) { 
				writer.print( "+(" +coeff[i] +" * x^" + i +")"); 
			}
			writer.close(); 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}		
	}
	
	public void writeDataToFile(File file) {
		PrintWriter writer;
		try {
			writer = new PrintWriter(file, "UTF-8");
			writer.println("x y g1x g2x ");
			List<Double> sortedKeys =  new ArrayList<Double>(trainingData.keySet());
			Collections.sort(sortedKeys);
			for(int i = 0; i < sortedKeys.size(); i++ ) { 
				double xi = sortedKeys.get(i); 
				writer.println ( xi + " " + trainingData.get(xi) + " " + g1b + " " + round(g2Prediction(xi))); 
			}
			writer.close(); 
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}		
	}

	public static double round(double d) { 
		return roundTo(d, 3); 
	}
	
	public static double roundTo(double d, int n) { 
		double result = d ; 
		long factor = Math.round(Math.pow(10, n)) ; 
		result = result * factor; 
		result = Math.round(result) ;
		result = result / factor; 
		return result ; 
	}

}
