package de.upb.ml1;

public abstract class ClassifierP2 extends Function<Boolean>{

	@Override
	public abstract Boolean evalPoint(Point p);
	
	@Override
	public abstract void train(Point p, Boolean b) ;

	@Override
	public Boolean trueEvaluation(Point p) {
		return p.x * p.x + p.y * p.y <= 1;
	}


}
