package hw.points;

public class Point {

 public double x, y ; 

 public Point() { 
  this.x = -1 + Math.random() * 2; 
  this.y = -1 + Math.random() * 2; 
 }

 public double calculateDistance(Point b) { 
  return Math.sqrt((x - b.x) *(x - b.x)  + (y - b.y)*(y - b.y) ); 
 }

 public String toString(){ 
  return " "+ x + " , " + y+ "  ";   
 }
	
 public Point(double x, double y) { 
  this.x = x ; 
  this.y = y; 
 }
}
