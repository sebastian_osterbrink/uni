package hw.points;


public abstract class Function {

	public abstract boolean evalPoint(Point p);

	public abstract boolean trueEvaluation(Point p);

	public abstract void  train(Point p,boolean b); 
	

}
