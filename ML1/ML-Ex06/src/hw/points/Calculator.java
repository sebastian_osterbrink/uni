package hw.points;

public class Calculator {

	public static void main(String[] args) {
		int base = 15600; 
		int step = 10; 
		for (int i= 0 ; i < 10 ; i++ ) { 			
			double n = base + i * step ; 
			double result = 0;
			result =  Math.pow(n, 4); 
			result = 640 * result + 40 ; 
			result = Math.log10(result);
			result = result * 8 / n; 
			result = Math.sqrt(result); 
			System.out.println(result);
		}
	}

}
