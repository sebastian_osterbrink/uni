package hw.points;


public class RectangleFunction extends Function {
	private double x_low	= 0; 
	private double x_high	= 0; 
	private double y_low 	= 0; 
	private double y_high 	= 0; 
	
	@Override
	public boolean evalPoint(Point p) {
		return x_low <=  p.x && p.x <= x_high  && y_low <= p.y && p.y <= y_high ;
	}
	
	public boolean trueEvaluation(Point p) { 
		return 		-0.7 <= p.x 
				&& 	 p.x <= 0  
				&& 	 0.3 <= p.y 
				&& 	 p.y <= 0.5;

	}
	@Override
	public void train(Point p ,boolean classification) { 
		if (classification) { 
			x_low = x_low == 0 ? p.x : x_low >= p.x ? p.x : x_low ; 
			y_low = y_low == 0 ? p.y : y_low >= p.y ? p.y : y_low; 
			x_high = x_high == 0 ? p.x : x_high <= p.x ? p.x : x_high; 
			y_high = y_high == 0 ? p.y : y_high <= p.y ? p.y : y_high; 
		}
	}
	
	public String toString() { 
		return "("+x_low+"|"+y_low +" )" + "("+x_high+"|"+y_high +" )"; 
	}
}
