import java.util.HashSet;
import java.util.Set;


public class Model {

	Set<Float> grades; 
	
	public void init() { 
		grades = new HashSet<Float>(); 
	}
	
	public void addGrade(float f) { 
		grades.add(new Float(f)); 
	}
	
	public float getAverage(){ 
		float result = 0; 
		for (Float  g : grades) {
			result += g; 
		}	
		return result / grades.size(); 
	}
	
	public int getGradeCount(){ 
		return grades.size();
	}
}
