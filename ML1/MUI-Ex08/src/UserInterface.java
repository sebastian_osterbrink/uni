import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserInterface {

	static Model m ; 
	public static void main (String[] args) {

		m = new Model(); 
		m.init();

		System.out.println("Calculation of the average degree ");
		System.out.print("Please enter the first degree:");

		boolean continueInput = true; 
		while (continueInput) { 
			// Open input 
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

			try {
				// read the grade 
				Float grade = Float.parseFloat(br.readLine());

				if (grade == 0 ) {  
					// case 1: input finished 
					continueInput = false;  
				} else if( 1 <= grade && grade <= 5 ) { 
					// case 2: valid input 
					m.addGrade(grade);
					System.out.print("Please enter the next degree or 0 to end:");
				} else { 
					// case 3: valid number, but not a valid grade 
					System.out.print(grade + " is not a valid grade. Please select a number between 1.0 and 5.0 or 0 to end: ");
			}   
			} catch (NumberFormatException nfe) { 
				// case 4: invalid number 
				System.out.print("Please enter a valid Number");		    	  
			} catch (IOException ioe) {
				// case 5: input error
				System.out.println("IO error trying to read the grade!");
				System.exit(1);
			}
		}
		System.out.println("The average of the "+m.getGradeCount()+" degrees is "+m.getAverage()); 
	}
}
