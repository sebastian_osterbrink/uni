import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;


public class Classifier {

	
	private final int DIM = 4; 
	
	
	private final int ERROR_INTERVAL = 2500; 
	private final int NUMBER_OF_RUNS= 20;

	private final Integer ALPHA_MAX = 10; 
	
	private  Map<Double[],Integer> alpha ; 
	
	private  Map<Double[],Integer> inputData; 
	private  Map<Integer,Long> errorRates ;	
	private  Map<Integer,Long> updateRates ;
	private  Map<Integer,Long> errorTotals ;
	private  Map<Integer,Long> updateTotals ;


	private  Random rand ;  


	
	public static void main(String[] args) throws FileNotFoundException, UnsupportedEncodingException {
		
		Classifier c = new Classifier(); 
	    	    
    	c.evalPerc(new File("perc.txt")); 

    	c.evalPercAlpha(new File("perc-noise.txt"));
	}
	
	
	public Classifier(){ 
		errorRates =  new HashMap<Integer,Long>();
		errorTotals =  new HashMap<Integer,Long>();
		updateTotals = new HashMap<Integer,Long>();
		updateRates = new HashMap<Integer,Long>();
		inputData = new HashMap<Double[],Integer>() ; 
		alpha = new HashMap<Double[],Integer>() ; 
		rand = new Random();

	}
	
	/** 
	 * Evaluate if the weighted sum of x is positive or negagive 
	 * @param x
	 * @param w
	 * @return
	 */
	private  int sign(Double[] x, Double[] w) { 
		double res  = 0; 
		for (int i = 0; i < x.length; i++) {
			res += x[i]*w[i]; 
		} 
		if (res < 0) { 
			return -1 ; 
		} else 
			return 1; 
	}
		
	/** 
	 * selects a random element from a set of objects 
	 * @param <E>
	 * @param objectSet
	 * @return
	 */
	private  <E> E getRandomObject(Set<E> objectSet) { 
		int i = 0; 
		int stop = rand.nextInt(objectSet.size()); 	
		for(E item : objectSet) {
		    if (i == stop) { 
		        return item; 
		    }
		    i++;
		}
		return null;	
	}

	/** 
	 * Transforms the input lines into a map of classifications
	 * @param inputLines
	 * @return
	 */
	private  Map<Double[],Integer> parseData(File file) { 
		List<String> inputLines; 
	    try {
	    	inputLines = Files.readAllLines(file.toPath(), Charset.defaultCharset());
		} catch (IOException e1) {
			e1.printStackTrace();
			inputLines = new ArrayList<String>(); 
		} 
	    
		Map<Double[],Integer> data = new HashMap<Double[], Integer>(); 
	    for (String line : inputLines) { 
	    	String[] splittedLine = line.split(" "); 

	    	// Parsing input data 
	    	try { 
    			Double[] inputParams= new Double[DIM+1];
    			inputParams[0] = 1.0 ; 
		    	for (int i = 1; i < splittedLine.length -1  ; i++) {
			        inputParams[i] = (Double.parseDouble(splittedLine[i])); 
				}
		    	// ignore empty lines 		    	
	    		if (splittedLine.length > 0 ) { 
	    			Integer classification = Integer.parseInt(splittedLine[splittedLine.length-1]); 
	    			data.put(inputParams, classification);    	
	    		}
	    	} catch ( NumberFormatException e )  { 
	    		System.out.println("Invalid data set");
	    	} 	 
	    }
	    return data; 
	}
	
	
	private void exportData(File f) throws FileNotFoundException, UnsupportedEncodingException { 
		PrintWriter writer = new PrintWriter(f, "UTF-8");		
		writer.println("t newErrors updates totalErrors totalUpdates");
		
		List<Integer> errorKeys = new ArrayList<Integer>(errorRates.keySet());
		Collections.sort(errorKeys);
		
		
		for (Integer interval : errorKeys) {
//			String uRate = ; 
//			String uTotal = ; 
			writer.println(interval + " " + 
							(errorRates.get(interval)/ NUMBER_OF_RUNS) + " "  + 
							( updateRates.isEmpty() ? "0 " : updateRates.get(interval)/NUMBER_OF_RUNS + " " ) + 
							(errorTotals.get(interval)/NUMBER_OF_RUNS)  + " "  + 
							( updateTotals.isEmpty() ? "0 " : updateTotals.get(interval)/NUMBER_OF_RUNS + " " ) 
						  ); 	
			
		}
		writer.close();
	}
	
	private  void evalPercAlpha(File file) throws FileNotFoundException, UnsupportedEncodingException { 
		errorRates =  new HashMap<Integer,Long>();
		errorTotals =  new HashMap<Integer,Long>();
		updateRates =  new HashMap<Integer,Long>();

	    inputData =  parseData(file); 
	 		
		for (int i = 0; i < NUMBER_OF_RUNS ; i++) {
			System.out.println("Run " + i);
			perceptonomAlpha(inputData);
		}
				
		exportData(new File("errorRateAlpha.csv"));		

	}
	
	private  void evalPerc(File file) throws FileNotFoundException, UnsupportedEncodingException { 
		errorRates =  new HashMap<Integer,Long>();
		errorTotals =  new HashMap<Integer,Long>();
		updateRates =  new HashMap<Integer,Long>();

	    inputData = parseData(file); 
		
		
		for (int i = 0; i < NUMBER_OF_RUNS ; i++) {
			System.out.println("Run " + i);
			perceptonom(inputData);
		}
				
		exportData(new File("errorRate.csv"));		
	}
	
	
	private  void perceptonomAlpha(Map<Double[], Integer> inputData) throws FileNotFoundException, UnsupportedEncodingException {
		
		Double[] w =  new Double[DIM+1]; 

		for (int i = 0; i < w.length; i++) {
			w[i] = 0.0;   
		}			
		
		Integer errors = 0;  
		Integer updates = 0; 
		Long errorTotal = new Long(0); 
		Long updateTotal = new Long(0); 

		for (int j = 0; j < ERROR_INTERVAL * 100 ; j++) {
			if (j > 0 && j % ERROR_INTERVAL == 0) {
				errorTotal += errors; 
				updateTotal += updates; 
				errorTotals.put(j,errorTotal + ( errorTotals.containsKey(j) ? errorTotals.get(j) : 0 ) ); 
				errorRates.put(j,errors + ( errorRates.containsKey(j) ? errorRates.get(j) : 0 ) ); 
				updateTotals.put(j,updateTotal + ( updateTotals.containsKey(j) ? updateTotals.get(j) : 0 ) ); 
				updateRates.put(j,updates + ( updateRates.containsKey(j) ? updateRates.get(j) : 0 ) ); 
				errors = 0; 
				updates= 0; 
			}
			Double[] xt  = getRandomObject(inputData.keySet()); 		
			int yHat = sign(xt,w); 
			int yt = inputData.get(xt); 
			
			if (yHat != yt) { 
				errors++; 
				if (!alpha.containsKey(xt))  { 
					alpha.put(xt, 0 ); 
				}
				if (alpha.get(xt) <= ALPHA_MAX) { 
					// Perform update 
					alpha.put(xt, alpha.get(xt) + 1  ); 
					for (int i = 0; i < w.length; i++) {
						w[i] = w[i] + ( ( yt - yHat ) / 2 ) * xt[i];   
					}	
					updates++; 
				} else { 
//					System.out.println(xt);
				}

			}	
		}
	}
		
	
	private void perceptonom(Map<Double[], Integer> inputData) throws FileNotFoundException, UnsupportedEncodingException {
			
		Double[] w =  new Double[DIM+1]; 

		for (int i = 0; i < w.length; i++) {
			w[i] = 0.0;   
		}			
		
		Integer errors = 0;  
		Long errorTotal = new Long(0); 
		for (int j = 0; j < ERROR_INTERVAL *100 ; j++) {
			if (j > 0 && j % ERROR_INTERVAL == 0) {
				errorTotal += errors; 
				errorTotals.put(j,errorTotal + ( errorTotals.containsKey(j) ? errorTotals.get(j) : 0 ) ); 
				errorRates.put(j,errors + ( errorRates.containsKey(j) ? errorRates.get(j) : 0 ) ); 
				errors = 0; 
			}
			Double[] xt  = getRandomObject(inputData.keySet()); 		
			int yHat = sign(xt,w); 
			int yt = inputData.get(xt); 
			if (yHat != yt) { 
				for (int i = 0; i < w.length; i++) {
					w[i] = w[i] + ( ( yt - yHat ) / 2 ) * xt[i];   
//					System.out.print(w[i] + " : " );
				}	
				errors++; 
//				System.out.println( j );
			}	
			
		}	
	
	}
	
	

}
