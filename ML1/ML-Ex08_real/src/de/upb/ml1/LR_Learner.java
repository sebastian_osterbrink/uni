package de.upb.ml1;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

public class LR_Learner {
	
	private Map<RealVector,Integer> trainingExamples = new HashMap<RealVector,Integer>();  
	
	private RealVector w ; 
	
	private int d = 5; 
	private double mu = 1;  
	
	public LR_Learner() { 
		w = new ArrayRealVector(d); 
	}
	
	public void addTrainingExample(RealVector x, int y) { 
		trainingExamples.put(x,y); 
	} 
	
	public void train(){ 
		
		RealVector nullvector = new ArrayRealVector(d); 
		RealVector deltaw = nullvector.mapAdd(1); 
		int t = 0 ; 
		while (nullvector.getDistance(deltaw) > 0.001) { 
			t++; 
			System.out.println(t + " : " +  deltaw);
			deltaw = new ArrayRealVector(d); 
			
			for(RealVector xn : trainingExamples.keySet()){ 
				int yn = trainingExamples.get(xn); 
				RealVector sumand = xn.mapMultiply(yn); 
				double exponent =  yn * w.dotProduct(xn); 
				double dividend = 1 + Math.exp(exponent) ; 
	
				deltaw = deltaw.add(sumand.mapDivide(dividend)); 
			}
			int N = trainingExamples.size();
			deltaw = deltaw.mapMultiply(mu / N ); 
			
			w = w.add(deltaw) ; 
		} 
		
	}

	public double confidencePositive(RealVector x) { 
		double wtx  = x.dotProduct(w); 		
		return Math.exp(wtx) / (1.0 + Math.exp(wtx)); 		
	}
	
	public int classify(RealVector x) { 
		boolean classification = confidencePositive(x) > 0.5; 
		return classification ? 1 : -1 ; 
	}
	
	public String toString(){ 
		return w.toString(); 
	}
	
	public int countErrors() {
		int errors = 0; 
		for(RealVector x : trainingExamples.keySet()){ 
			if (!trainingExamples.get(x).equals(classify(x)))  { 
				errors++; 
			}
		}
		return errors; 
	}
	
	public int countTrainingExamples() {
		return trainingExamples.size(); 
	}
		
}
