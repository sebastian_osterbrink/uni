package de.upb.ml1;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

public class Main {

	public static void main(String[] args) throws IOException {
		LR_Learner learner = new LR_Learner(); 
		
		Reader in = new FileReader("lr-data.csv");
		CSVFormat hwFormat = CSVFormat.newFormat(' '); 
		hwFormat = hwFormat.withIgnoreSurroundingSpaces(true); 
		hwFormat = hwFormat.withIgnoreEmptyLines(); 
		Iterable<CSVRecord> records = CSVFormat.DEFAULT.withHeader().withIgnoreSurroundingSpaces().parse(in);
		
		for (CSVRecord record : records) {
			double[] values = new double[5]; 
			values[0] = 1; 
			for (int i = 1; i < values.length; i++) {
				values[i]  = Double.parseDouble(record.get(i)); 
			}
			int y = Integer.parseInt(record.get(record.size()-1)); 
			RealVector x = new ArrayRealVector(values); 

			learner.addTrainingExample( x, y);
//			System.out.println(record);
		}
		learner.train();
		
		System.out.println(learner);
		System.out.println(learner.countErrors() + " of " + learner.countTrainingExamples() );
	}

}
