package hw.data;

public class Yeast {
	public String name; 
	public Double[] data; 
	
	public Yeast(String name, Double[] data) { 
		this.name = name; 
		this.data = data; 
	}
	
}
