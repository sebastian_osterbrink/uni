package hw.data;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DataConverter {

	public static void main(String[] args) {	
		List<String> yeastDataLines = readFile(new File("yeast.data")); 
		Map<Yeast, String> yeastClassifications = parseYeasts(yeastDataLines, "  "); 
		exportYeastData(new File("output/yeast.arff"), yeastClassifications); 
		
		List<String> zooDataLines = readFile(new File("zoo.data")); 
		Map<Animal, Integer> zooClassifications = parseZoo(zooDataLines, ","); 
		exportZooData(new File("output/zoo.arff"), zooClassifications); 

	}

	
	/** 
	 * Read the input lines 
	 * @param inputLines
	 * @return
	 */
	private static  List<String> readFile(File file) { 
		List<String> inputLines; 
	    try {
	    	inputLines = Files.readAllLines(file.toPath(), Charset.defaultCharset());
		} catch (IOException e1) {
			e1.printStackTrace();
			inputLines = new ArrayList<String>(); 
		} 
	    return inputLines; 
	}
	
	private static Map<Yeast, String> parseYeasts(List<String> dataLines , String seperator) { 
		Map<Yeast, String> classifications = new HashMap<Yeast,String>(); 
		for(String line : dataLines) { 
			List<String> sepLineItems = new ArrayList<String>(); 
			while( !line.isEmpty()) { 
				int cut = line.indexOf(" ");
				String subStr;
				if (cut == -1 ) { 
					subStr = line.trim();
					line = ""; 
				} else { 
					subStr = line.substring( 0, cut);
					line = line.substring(cut).trim(); 					
				}
				sepLineItems.add(subStr); 
				System.out.print(subStr+" ");
			}
			System.out.println();
			
			Double[] data = new Double[8]; 
			for (int i = 0; i< 8 ; i++) { 
				data[i] = Double.parseDouble(sepLineItems.get(i+1)); 
			}
			Yeast item = new  Yeast(sepLineItems.get(0),data) ; 
			classifications.put( item, sepLineItems.get(9)); 
			
		}
		return classifications; 
	}
	
	private static void exportYeastData(File file, Map<Yeast, String> classifications) { 
		PrintWriter writer;
		try {
			writer = new PrintWriter(file, "UTF-8");
			writer.println(" @RELATION yeast_data" ); 
			writer.println(" @ATTRIBUTE name  string" ); 
			writer.println(" @ATTRIBUTE mcg  NUMERIC" ); 
			writer.println(" @ATTRIBUTE gvh  NUMERIC" ); 
			writer.println(" @ATTRIBUTE alm  NUMERIC" ); 
			writer.println(" @ATTRIBUTE mit  NUMERIC" ); 
			writer.println(" @ATTRIBUTE erl  NUMERIC" ); 
			writer.println(" @ATTRIBUTE pox  NUMERIC" ); 
			writer.println(" @ATTRIBUTE vac  NUMERIC" ); 
			writer.println(" @ATTRIBUTE nuc  NUMERIC" ); 
			writer.println(" @ATTRIBUTE class  {CYT,NUC,MIT,ME3, ME2,ME1,EXC,VAC,POX,ERL}" ); 
			writer.println(" @DATA" ); 
			for(Yeast y : classifications.keySet()) { 
				writer.print(y.name + ","); 
				for (int i = 0; i < y.data.length; i++) {
					writer.print(y.data[i] + ","); 
				}
				writer.println(classifications.get(y)); 
			}
			writer.close(); 
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}	
	}

	
	private static Map<Animal, Integer> parseZoo(List<String> dataLines , String seperator) { 
		Map<Animal, Integer> classifications = new HashMap<Animal,Integer>(); 
		for(String line : dataLines) { 
			List<String> sepLineItems = new ArrayList<String>(); 
			while( !line.isEmpty()) { 
				int cut = line.indexOf(seperator);
				String subStr;
				if (cut == -1 ) { 
					subStr = line.trim();
					line = ""; 
				} else { 
					subStr = line.substring( 0, cut);
					line = line.substring(cut+1).trim(); 					
				}
				sepLineItems.add(subStr); 
				System.out.print(subStr+" ");
			}
			System.out.println();
			
			int max = 17; 
			Integer[] data = new Integer[max]; 
			for (int i = 0; i< max; i++) { 
				data[i] = Integer.parseInt(sepLineItems.get(i+1)); 
			}
			Animal item = new  Animal(sepLineItems.get(0),data) ; 
			classifications.put( item, Integer.parseInt(sepLineItems.get(max))); 
			
		}
		return classifications; 
	}
	
	private static void exportZooData(File file, Map<Animal, Integer> zooClassifications) { 
		PrintWriter writer;
		try {
			writer = new PrintWriter(file, "UTF-8");
			writer.println(" @RELATION zoo_data" ); 
			writer.println(" @ATTRIBUTE name  string" ); 
			writer.println(" @ATTRIBUTE hair  {0,1}" ); 
			writer.println(" @ATTRIBUTE feathers  {0,1}" ); 
			writer.println(" @ATTRIBUTE eggs  {0,1}" ); 
			writer.println(" @ATTRIBUTE milk  {0,1}" ); 
			writer.println(" @ATTRIBUTE airborne  {0,1}" ); 
			writer.println(" @ATTRIBUTE aquatic  {0,1}" ); 
			writer.println(" @ATTRIBUTE predator  {0,1}" ); 
			writer.println(" @ATTRIBUTE toothed  {0,1}" ); 
			writer.println(" @ATTRIBUTE backbone  {0,1}" ); 
			writer.println(" @ATTRIBUTE breathes  {0,1}" ); 
			writer.println(" @ATTRIBUTE venomous  {0,1}" ); 
			writer.println(" @ATTRIBUTE fins  {0,1}" ); 
			writer.println(" @ATTRIBUTE legs  {0,2,4,5,6,8}" ); 
			writer.println(" @ATTRIBUTE tail  {0,1}" ); 
			writer.println(" @ATTRIBUTE domestic  {0,1}" ); 
			writer.println(" @ATTRIBUTE catsize  {0,1}" ); 
			writer.println(" @ATTRIBUTE type {1,2,3,4,5,6,7}" ); 
			writer.println(" @DATA" ); 
		
			for(Animal a : zooClassifications.keySet()) { 
				writer.print(a.name + ","); 
				for (int i = 0; i < a.data.length; i++) {
					writer.print(a.data[i] + ","); 
				}
				writer.println(zooClassifications.get(a)); 
			}
			writer.close(); 
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			e.printStackTrace();
		}	
	}

}
