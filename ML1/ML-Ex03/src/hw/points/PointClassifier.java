package hw.points;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class PointClassifier {
		
	
	public static void main(String[] args ) throws FileNotFoundException, UnsupportedEncodingException{ 
		
		Function f1 =  new Function() {
			@Override
			public boolean evalPoint(Point p) {
				return Math.abs(p.x) <= 1- Math.abs(p.y);
			} 
		}; 
		
		Function f2 =  new Function() {
			@Override
			public boolean evalPoint(Point p) {
				return Math.abs(p.x) <= 0.5 && Math.abs(p.y) <= 0.5;
			} 
		}; 
			
		
		exportSample(makeSample(1000,f1)  ,new File(  "output/D1.arff"), "F1_Samples");
		
		exportSample(makeSample(1000,f2) , new File(  "output/D2.arff"), "F2_Samples");

	}


	public static double round(double d) { 
		return roundTo(d, 3); 
	}
	
	public static double roundTo(double d, int n) { 
		double result = d ; 
		long factor = Math.round(Math.pow(10, n)) ; 
		result = result * factor; 
		result = Math.round(result) ;
		result = result / factor; 
		return result ; 
	}
	
	
	
	
	public static boolean evalPoint(Point p) { 
		return ( p.x * p.x  + p.y * p.y)  <= 0.3 ;  
	}
	
	
	
	public static Map<Point, Boolean> makeSample(int n, Function f){ 
		Map<Point,Boolean> data = new HashMap<Point,Boolean>();
		for (int i = 0; i < n; i++) {
			Point p = new Point(); 
			Boolean b = f.evalPoint(p);  
			data.put(p,b); 
		}		
		return data ; 
	}
	
	private static void exportSample(Map<Point, Boolean> data, File f, String relationName) throws FileNotFoundException, UnsupportedEncodingException { 
		PrintWriter writer = new PrintWriter(f, "UTF-8");		
		writer.println(" @RELATION " + relationName); 
		writer.println(" @ATTRIBUTE x_1  NUMERIC" ); 
		writer.println(" @ATTRIBUTE x_2  NUMERIC" ); 
		writer.println(" @ATTRIBUTE class  {+1, -1 }" ); 

		writer.println(" @DATA" ); 

		for (Point p : data.keySet()) {
			writer.println( p.x + "," + p.y + "," + ( data.get(p) ? "+1" : "-1" ) ); 	
			
		}
		writer.close();
	}
	
	
}
