package hw.points;

public abstract class Function {

	public abstract boolean evalPoint(Point p);
	
}
