import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Classifier {
		
	static HashMap<Point, Boolean> trainingData ; 
	static HashMap<Point, Boolean> classificationMap; 
	static HashMap<Point, Point> nearestPoint; 
	static List<Point> queries ; 
	
	public static void main(String[] args ) throws FileNotFoundException, UnsupportedEncodingException{ 
		makeSample(10); 
		makeQueries(10);
		
		classNN(queries, trainingData); 

		
		PrintWriter writer ; 

		writer = new PrintWriter("trainingData.tex", "UTF-8");		
		writer.println(" x &  y &  classification \\\\ ");
		for(Point point : trainingData.keySet()){ 
			writer.println(point.x + " & " + point.y + " & " + (trainingData.get(point) ? "1" : "-1") + "\\\\" );
		}
		writer.close();
			
		writer = new PrintWriter("queryDataNN.tex", "UTF-8");		
		writer.println("x & y & classification & correctnes &  & x & y & classification  \\\\");
		for(Point point : classificationMap.keySet()){ 
			Point p = nearestPoint.get(point) ; 
			writer.println(round(point.x) + "&" + round(point.y)+ " & " 
					+ (classificationMap.get(point) ? "1" : "-1") + " & "
					+ (classificationMap.get(point) == Classifier.evalPoint(point) ? "CORRECT" : "MISS") 
					+ "&  & " + round(p.x) + "&" + round(p.y)+ " & " 
					+ (trainingData.get(p)? "1" : "-1 ") 
					+ "\\\\");
		}
		writer.close();
		
		classBOX(queries, trainingData); 
		
		writer = new PrintWriter("queryDataBOX.tex", "UTF-8");		
		writer.println("x & y & classification & correctnes \\\\");
		for(Point point : classificationMap.keySet()){ 
			writer.println(round(point.x) + "&" + round(point.y)+ " & " 
					+ (classificationMap.get(point) ? "1" : "-1") + " & "
					+ (classificationMap.get(point) == Classifier.evalPoint(point) ? "CORRECT" : "MISS") 
					+ "\\\\");
		}
		writer.close();
		
		
		
		// New DATA set 				
		writer = new PrintWriter("errorRate2b.tex", "UTF-8");		
		for(int j = 0 ; j < 11 ; j++ ) { 
			Point testPoint = new Point(j * 0.1 ,j * 0.1 ); 
			writer.print(round(testPoint.x) + " , " + round(testPoint.y)); 
			for (int i = 1; i < 5; i++) {
				double er = errorRate(testPoint, i *10 ); 
				er = er * 10000; 
				er = Math.round(er) ; 
				er = er / 100 ; 
				writer.print( " & " + er + "\\%" );					
			}			
			writer.println(" \\\\ "); 
		}
		writer.close();
		
		writer = new PrintWriter("errorRate3b.tex", "UTF-8");		
		for(int j = 0 ; j < 11 ; j++ ) { 
				writer.print( "n=" +j*10 );					
				double er = roundTo( errorRateEval(j*10, true ) *100 , 3); 
				writer.print( " & " + er + "\\%" );					
				er = roundTo(errorRateEval(j*10, false ) *100 ,3);  
				writer.print( " & " + er + "\\%" );		
				writer.println("\\\\");
		}
		writer.close();

	}

	public static double round(double d) { 
		return roundTo(d, 3); 
	}
	
	public static double roundTo(double d, int n) { 
		double result = d ; 
		long factor = Math.round(Math.pow(10, n)) ; 
		result = result * factor; 
		result = Math.round(result) ;
		result = result / factor; 
		return result ; 
	}
	
	public static boolean evalPoint(Point p) { 
		return ( p.x * p.x  + p.y * p.y)  <= 0.3 ;  
	}
	
	public static double errorRate(Point x, int n ){ 
		queries = new ArrayList<Point>(); 
		queries.add(x); 
		double result = 0; 
		for(int i = 0; i< 3000 ; i++ ){ 
			trainingData = makeSample(n); 
			classNN(queries, trainingData); 		
			if (evalPoint(x) != classificationMap.get(x)){ 
					result ++ ;
			} 
		}
		return result / 3000 ; 
	}
	
	public static double errorRateEval(int sampleSize , boolean useBOX ){ 
		int numberOfRuns = 5000; 
		int numberOfQueries = 100; 
		double result = 0; 
		for(int i = 0; i< numberOfRuns ; i++ ){ 
			queries = makeQueries(100); 
			trainingData = makeSample(sampleSize); 
			classificationMap = useBOX ? classBOX(queries, trainingData) : classNN(queries, trainingData) ; 		
			for (Point p : classificationMap.keySet()) { 
				if (classificationMap.get(p) != evalPoint(p)) { 
					result ++ ; 
				}
			}
		}
		return result / ( numberOfRuns * numberOfQueries) ;  
	}
	
	
	public static List<Point> makeQueries(int n) { 
		queries = new ArrayList<Point>(); 
		for (int i = 0; i < n; i++) {
			queries.add(new Point()); 
		}
		return queries; 
	}
	
	public static HashMap<Point, Boolean> makeSample(int n){ 
		trainingData = new HashMap<Point,Boolean>();
		for (int i = 0; i < n; i++) {
			Point p = new Point(); 
			Boolean b = evalPoint(p);  
			trainingData .put(p,b); 
		}		
		return trainingData ; 
	}
	
	
	public static HashMap<Point,Boolean> classBOX(List<Point> points , HashMap<Point,Boolean> trainingData) { 
		classificationMap = new HashMap<Point,Boolean>();
		double a1= 1; 
		double a2= 1; 
		double b1= -1; 
		double b2= -1; 
		for (Point p: trainingData.keySet()) { 
			if (trainingData.get(p)){ 
				a1 = a1 > p.x ? p.x : a1; 
				a2 = a2 > p.y ? p.y : a2; 
				b1 = b1 < p.x ? p.x : b1; 
				b2 = b2 < p.y ? p.y : b2; 
			}		
		}
		for (Point p: points) { 
			boolean found = a1 <= p.x && a2 <= p.y && b1 >= p.x && b2 >= p.y; 
			classificationMap.put(p, found); 
		}		
		return classificationMap; 
	}
	
	
	public static HashMap<Point,Boolean> classNN(List<Point> points , HashMap<Point,Boolean> trainingData) { 
		classificationMap = new HashMap<Point,Boolean>();
		nearestPoint = new HashMap<Point,Point>(); 
		
		for (Point point : points) {
			Boolean classification = false; 
			double minDistance = 2; 
			Point foundNeighbour = null; 
			for(Point classedPoint : trainingData.keySet()){ 
				double newDistace = point.calculateDistance(classedPoint); 
				if( minDistance > newDistace) { 
					minDistance = newDistace; 
					classification = trainingData.get(classedPoint); 
					foundNeighbour = classedPoint; 
				}
			}
			classificationMap.put(point,classification); 
			nearestPoint.put(point, foundNeighbour); 
		}

		return classificationMap; 
	}
	
}
