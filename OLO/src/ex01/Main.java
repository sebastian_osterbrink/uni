/**
 * 
 */
package ex01;

import org.apache.commons.math3.distribution.BinomialDistribution;
import org.apache.commons.math3.stat.descriptive.moment.Mean;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

/**
 * @author Sebastian Osterbrink
 *
 */
public class Main {

	private static int k = 10 ;
	private static double epsilon = 0.1; 
	private static double p = 0.7; 
	
	private static int kappa ; 
	private static double delta; 

	private static int repeats = 50; 
	private static double[] stepCounts ; 
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		stepCounts = new double[repeats];  	
		for (int i = 0; i < repeats; i++) {
			a(i);
		}
		System.out.println("a) average step count: " + new Mean().evaluate(stepCounts) + 
				" Standart deviation: " + new StandardDeviation().evaluate(stepCounts) );

		stepCounts = new double[repeats];  	
		for (int i = 0; i < repeats; i++) {
			b(i);
		}	
		System.out.println("b) average step count: " + new Mean().evaluate(stepCounts) + 
				" Standart deviation: " + new StandardDeviation().evaluate(stepCounts) );
		
		stepCounts = new double[repeats];  	
		for (int i = 0; i < repeats; i++) {
			c(i);
		}	
		System.out.println("b) average step count: " + new Mean().evaluate(stepCounts) +
				" Standart deviation: " + new StandardDeviation().evaluate(stepCounts) );
		
	}

	private static void a(int repeat){
		BinomialDistribution[] bandits = new BinomialDistribution[k]; 
		for (int i = 0; i < bandits.length; i++) {
			bandits[i] = new BinomialDistribution(1, p);  
		}
		bandits[(int)(Math.random() * k)] = new BinomialDistribution(1, p + epsilon); 
		
		int nMax = 5000; 
		kappa = 1 ; 
		delta = 0.05; 
		RacingAlgorithm algo = new RacingAlgorithm(bandits, kappa, nMax, delta); 
		stepCounts[repeat] = algo.getFinishedStep();
//		System.out.println(algo.getFinishedStep());
	}
	
	private static void b(int repeat) { 
		BinomialDistribution[] bandits = new BinomialDistribution[k]; 
		for (int i = 0; i < bandits.length; i++) {
			bandits[i] = new BinomialDistribution(1, p);  
		}
		bandits[(int)(Math.random() * k)] = new BinomialDistribution(1, p + epsilon); 
		
		RacingAlgorithm algo = null; 
		boolean finished = false; 
		int nMax = 1; 
		while (!finished) {
			nMax *= 2; 
			kappa = 1 ; 
			delta = 0.05; 
			algo = new RacingAlgorithm(bandits, kappa, nMax, delta); 
			finished = algo.isFinished(); 
		}
		stepCounts[repeat] = algo.getFinishedStep();
//		System.out.println("finished doubling with nMax=" +nMax + " in step "+ algo.getFinishedStep());
	}
	
	private static void c(int repeat) {
		BinomialDistribution[] bandits = new BinomialDistribution[k]; 
		for (int i = 0; i < bandits.length; i++) {
			bandits[i] = new BinomialDistribution(1, p);  
		}
		bandits[(int)(Math.random() * k)] = new BinomialDistribution(1, p + epsilon); 
		
		int nMax = 5000; 
		kappa = 1 ; 
		delta = 0.05; 
		Racing2Algorithm algo = new Racing2Algorithm(bandits, kappa, nMax, delta); 
				
		stepCounts[repeat] = algo.getFinishedStep();
//		System.out.println(algo.getFinishedStep());
	}
}
