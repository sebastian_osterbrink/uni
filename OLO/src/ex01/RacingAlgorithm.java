package ex01;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.math3.distribution.BinomialDistribution;

public class RacingAlgorithm {

	private Set<Integer> selected; 
	private Set<Integer> discarded; 
	private Set<Integer> undecided; 
	private BinomialDistribution[] bandits ;
	
	private int[][] history; 
	private double[][] avg; 
	private double[][] confidence; 

	private int k; 
	private int nMax; 
	private double delta; 
	private int kappa ; 
	private int tFinished = 0; 
	
	public RacingAlgorithm(BinomialDistribution[] bandits, int kappa, int nMax, double delta) { 
		this.k = bandits.length; 
		this.bandits = bandits;
		this.nMax = nMax; 
		this.delta = delta; 
		this.kappa = kappa ; 
		
		selected = new HashSet<>(); 
		discarded = new HashSet<>();
		undecided = new HashSet<>(); 
		history = new int[k][nMax]; 
		avg = new double[k][nMax]; 
		confidence = new double[k][nMax]; 
		
		for (int i = 0; i < k; i++) {
			undecided.add(new Integer(i)); 
			for (int j = 0; j < nMax; j++) {
				history[i][j] = 0; 
				avg[i][j] = 0; 
			} 
		}
		run(nMax); 
	}
	
	public boolean isFinished() {
		return selected.size() == kappa; 
	}
	
	public int getFinishedStep() { 
		return tFinished; 
	}
	
	public String run(int nMax) { 		
		int t = 1 ; 
		while ((t< nMax)&& selected.size() < kappa) {
			double conf = Math.sqrt(Math.log(2.0 * k * nMax / delta) / (2*t)); 
//			System.out.println("t="+t +  " confidence=" + conf );

			for (Integer i : undecided) {
				history[i][t] = bandits[i].sample();
				double average = 0; 
				for (int j = 0; j < t; j++) {
					average += history[i][j]; 
				}
				avg[i][t] = average / t ; 
				confidence[i][t] = conf; 
			}
			List<Integer> noLongerUndecided = new ArrayList<Integer>(); 
			for (Integer i : undecided) {
				double thisLowerBound = avg[i][t] - confidence[i][t];
				double thisUpperBound = avg[i][t] + confidence[i][t];
//				System.out.print(" i:"+i); 
//				System.out.print(" average:"+avg[i][t] ); 
//				System.out.print(" confidence:"+confidence[i][t] ); 
//				System.out.print(" ["+thisLowerBound + " | " + thisUpperBound+"]");
				Set<Integer> betterThanThis = new HashSet<Integer>(); 
				Set<Integer> worseThanThis = new HashSet<Integer>(); 
				for (Integer j : undecided) {
					if (thisLowerBound > avg[j][t] + confidence[j][t]){ 
						worseThanThis.add(j); 
					}
					if (thisUpperBound < avg[j][t] - confidence[j][t]){ 
						betterThanThis.add(j); 
					}
				}
//				System.out.println (" Better than:" + worseThanThis + " Worse than:" + betterThanThis); 
				if (worseThanThis.size() >= k - kappa - discarded.size()){ 
					selected.add(i); 
					noLongerUndecided.add(i);
				} else if (betterThanThis.size() >= kappa - selected.size()){ 
					discarded.add(i); 
					noLongerUndecided.add(i);
				}
			}
			undecided.removeAll(noLongerUndecided); 		
//			System.out.println(" selected:" +selected +" discarded:"+ discarded +" undecided:"+ undecided);
			
			t++; 
			
		} 
		tFinished = t; 
		String result = "t="+t ; 
//		result += " selected: " + selected ; 
		return result; 
	}
}
