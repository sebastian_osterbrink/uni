package de.upb.comp.huffman;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;

/**
 * Main class of the Huffman package. Allows to calculate a Huffman encoding for
 * an input String *
 */
public class Huffman {
	
	/**
	 * Computes the frequency distribution of a given String
	 * 
	 * @param in
	 *            The input String
	 * @return A mapping of characters to their frequencies
	 */
	private Hashtable<Character, Integer> computeFrequencies(String in) {
		Hashtable<Character, Integer> frequencies = new Hashtable<Character, Integer>();

		for (int i = 0; i < in.length(); i++) {
			Character currentC = in.charAt(i);
			if(frequencies.containsKey(currentC)) { 
				frequencies.put(currentC , frequencies.get(currentC)+1); 
			} else { 
				frequencies.put(currentC, 1); 
			}
			
		}
		System.out.println(frequencies);
		return frequencies;
	}

	/**
	 * Computes the Huffman tree for a given frequency distribution
	 * 
	 * @param frequencies
	 *            Mapping of characters to their frequencies
	 * @return The root of the Huffman tree
	 */
	private HuffmanTreeNode computeTree(
			Hashtable<Character, Integer> frequencies) {
		PriorityQueue<HuffmanTreeNode> huffmanForest = new PriorityQueue<HuffmanTreeNode>(
				frequencies.size());

		for (Character key : frequencies.keySet()) {
			huffmanForest.add(new HuffmanTreeNode(key, frequencies.get(key)));
		}
		
		while( huffmanForest.size() > 1 ) { 
			HuffmanTreeNode lowest = huffmanForest.poll(); 
			HuffmanTreeNode secondLowest = huffmanForest.poll();
			huffmanForest.add(new HuffmanTreeNode(lowest, secondLowest)); 
		}
		//TODO: implement this method: At the end the huffmanForest should only contain the root node of the Huffman tree
		
		
		HuffmanTreeNode r = huffmanForest.poll();
		storeCodings(r, new ArrayList<Boolean>());

		return r;
	}
	
	/**
	 *  Encodes a given String	 
	 * @param in The input String
	 * @param root The Huffman tree
	 * @param ret List of Boolean representing the resulting
	 *            Huffman code
	 */
	private void encode(String in, HuffmanTreeNode root, List<Boolean> ret){
		
		//TODO: implement this method: store the encoding of input String in as a List of booleans in variable "ret"
		for (int i = 0; i < in.length(); i++) {
			Character currentChar = in.charAt(i); 
			ret.addAll(codings.get(currentChar)); 
		}
	
	}
	
	


	/**
	 * Computes the Huffman tree and encodes a given String
	 * 
	 * @param in
	 *            The input String
	 * @param ret
	 *            A List of Boolean values in which to store the resultung
	 *            Huffman code
	 * @return The root node of the Huffman tree
	 */
	public HuffmanTreeNode encode(String in, List<Boolean> ret) {

		Hashtable<Character, Integer> frequencies = computeFrequencies(in);

		HuffmanTreeNode root = computeTree(frequencies);

		if (debug)
			printCoding(root);

		encode(in, root, ret);

		return root;
	}
	

	/**
	 * Decodes a Huffman coded String
	 * 
	 * @param code
	 *            The Huffman code of the String to decode
	 * @param root
	 *            The Huffman tree
	 * @return The decoded String
	 */
	public String decode(ArrayList<Boolean> code, HuffmanTreeNode root) {
		String ret = "";

		HuffmanTreeNode currentNode = root;

		for (int i = 0; i < code.size(); i++) {
			if (code.get(i)) { 
				currentNode = currentNode.getRight(); 
			} else { 
				currentNode = currentNode.getLeft();
			}
			if (currentNode.isLeaf()) { 
				ret += currentNode.getValue();
				currentNode = root; 
			} 
		} 
		System.out.println(ret);
		
		//TODO: implement this method

		return ret;
	}
	
	/**
	 * Recursively prints the Huffman tree
	 * 
	 * @param node
	 *            The current node of the tree
	 * @param code
	 *            The code that corresponds to the path from the root node to
	 *            the current node
	 */
	private void printCode(HuffmanTreeNode node, String code) {
		if (node.isLeaf()) {
			System.out.println(node.getValue() + " " + code);
		} else {
			printCode(node.getLeft(), code + "0");
			printCode(node.getRight(), code + "1");
		}
	}

	/**
	 * "Inverses" the Huffman tree and creates a mapping of characters to their
	 * Huffman code
	 * 
	 * @param node
	 *            The current node of the Huffman tree
	 * @param currentCode
	 *            The code that corresponds to the path from the root node to
	 *            the current node
	 */
	private void storeCodings(HuffmanTreeNode node,
			ArrayList<Boolean> currentCode) {
		if (node.isLeaf()) {

			codings.put(node.getValue(), currentCode);

		} else {
			@SuppressWarnings("unchecked")
			ArrayList<Boolean> codingForLeft = (ArrayList<Boolean>) currentCode
					.clone();
			codingForLeft.add(false);
			storeCodings(node.getLeft(), codingForLeft);

			@SuppressWarnings("unchecked")
			ArrayList<Boolean> codingForRight = (ArrayList<Boolean>) currentCode
					.clone();
			codingForRight.add(true);
			storeCodings(node.getRight(), codingForRight);
		}
	}

	/**
	 * Prints the Huffman tree to console. Allows to verify the generated
	 * Huffman tree.
	 * 
	 * @param root
	 *            The root of the Huffman tree to be printed
	 */
	public void printCoding(HuffmanTreeNode root) {
		printCode(root, "");
	}

	/**
	 * Mapping of characters to their Huffman code
	 */
	private Hashtable<Character, ArrayList<Boolean>> codings = new Hashtable<Character, ArrayList<Boolean>>();

	/**
	 * A flag that indicates whether or not debug output shall be printed to
	 * console
	 */
	private final boolean debug = true;

	/**
	 * "Roundtrip": String S is encoded to code(S) and code(S) is decoded to S'.
	 * Finally S and S' are compared and the compression ratio is written to
	 * console
	 * 
	 * @param s
	 *            The input String
	 */
	public void roundtrip(String s) {
		ArrayList<Boolean> code = new ArrayList<Boolean>();

		HuffmanTreeNode root = encode(s, code);

		double compRatio = (1.0 * code.size()) / (s.length() * 8.0) * 100.0;

		String decomp = decode(code, root);

		if (s.equals(decomp))
			System.out.println("Compression ratio: " + compRatio + "%");
		else
			System.out.println("Compression failed!");

	}

	/**
	 * Reads the content of a file into a String
	 * 
	 * @param file
	 *            The file name
	 * @return The String containing the file's content
	 * @throws IOException
	 */
	private static String readFile(String file) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = null;
		StringBuilder stringBuilder = new StringBuilder();
		String ls = System.getProperty("line.separator");

		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
			stringBuilder.append(ls);
		}

		reader.close();

		return stringBuilder.toString();
	}

	public static void main(String args[]) {
		if (args.length < 1) {
			System.out
					.println("Usage: Huffman (<String to compress> | -i <File to compress>");
			return;
		}
		String s = "";
		if (args.length == 2 && args[0].equals("-i")) {
			try {
				s = Huffman.readFile(args[1]);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else
			s = args[0];

		Huffman h = new Huffman();
		h.roundtrip(s);
		
		h.roundtrip("tobeornottobeortobeornot");
		h.roundtrip("peterpiperpickedapeckofpickledpeppersapeckofpickledpepperspeterpiperpickedifpeterpiperpickedapeckofpickledpepperswheresthepeckofpickledpepperspeterpiperpicked");
		h.roundtrip("iwishtowishthewishyouwishtowishbutifyouwishthewishthewitchwishesiwontwishthewishyouwishtowish");
		h.roundtrip("howmanycookiescouldagoodcookcookifagoodcookcouldcookcookiesagoodcookcouldcookasmuchcookiesasagoodcookwhocouldcookcookies");
		
		
//		List<Boolean> result = new ArrayList<Boolean>() ; 
//		h.encode("robot", null, result);
//		
//		for (Boolean b : result) { 
//			System.out.print(( b? 1 : 0));			
//		}
//		
	}

}
