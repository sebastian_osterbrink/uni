package de.upb.comp.huffman;
/**
 * Represents a single node of the Huffman tree
 *
 */
public class HuffmanTreeNode implements Comparable<HuffmanTreeNode> {
	private int prob;
	private Character value;
	private HuffmanTreeNode left=null;
	private HuffmanTreeNode right=null;
	/**
	 * Constructor, generates an inner node with two child nodes
	 * @param child0 The first child node that is reached via a 0-bit
	 * @param child1 The second child node that is reached via a 1-bit
	 */
	public HuffmanTreeNode(HuffmanTreeNode child0, HuffmanTreeNode child1)
	{
		value=null;		
		prob=child0.prob+child1.prob;
		left=child0;
		right=child1;
	}
	/**
	 * Constructor, generates a leaf node with an element and a probability
	 * @param value The element value
	 * @param prob The probability
	 */
	public HuffmanTreeNode(Character value, int prob)
	{		
		this.value=value;
		this.prob=prob;
	}
	/**
	 * Returns the probability, i.e., the probability of the element for a leaf node and the sum of the probabilities of the child nodes for an inner node
	 * @return The probability
	 */
	public int getProb()
	{
		return prob;
	}
	/**
	 * Returns the value, i.e., the value of the element for a leaf node and null for an inner node
	 * @return The value
	 */
	public Character getValue()
	{
		return value;
	}
	/**
	 * Checks, whether the current node is a leaf node
	 * @return true - if this node is a leaf node<br> false - otherwise
	 */
	public boolean isLeaf()
	{
		return (left==null)||(right==null);
	}
	
	/**
	 * Returns the left child of this node or null if this node is a leaf node
	 * @return The left child or null for leaf nodes
	 */
	public HuffmanTreeNode getLeft()
	{
		return left;
	}
	/**
	 * Returns the right child of this node or null if this node is a leaf node
	 * @return The right child or null for leaf nodes
	 */
	
	public HuffmanTreeNode getRight()
	{
		return right;
	}
	
/**
 * Compares this object with the specified object for order. Returns a negative integer, zero, or a positive integer as this object is less than, equal to, or greater than the specified object. 
 */
	@Override
	public int compareTo(HuffmanTreeNode o) {
		if(o.prob<this.prob)return 1;
		if(o.prob>this.prob)return -1;		
		return 0;
	}
	
	public String toString(){
		if(isLeaf()) return "'" + value + "'";
		return "(0: " + left + ", 1:" + right + ")";
	}
}
